package com.sample.app.weatherapp

import android.os.Build
import com.sample.app.weatherapp.dao.CitiesForSearchDaoTest
import com.sample.app.weatherapp.dao.CurrentWeatherDaoTest
import com.sample.app.weatherapp.dao.ForecastDaoTest
import com.sample.app.weatherapp.repo.CurrentWeatherRepositoryTest
import com.sample.app.weatherapp.repo.ForecastRepositoryTest
import com.sample.app.weatherapp.viewModel.DashboardViewModelTest
import com.sample.app.weatherapp.viewModel.SearchViewModelTest
import com.sample.app.weatherapp.viewModel.WeatherDetailViewModelTest
import com.sample.app.weatherapp.viewState.CurrentWeatherViewStateTest
import com.sample.app.weatherapp.viewState.ForecastViewStateTest
import com.sample.app.weatherapp.viewState.SearchViewStateTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(Suite::class)
@Suite.SuiteClasses(
    CitiesForSearchDaoTest::class,
    CurrentWeatherDaoTest::class,
    CurrentWeatherViewStateTest::class,
    DashboardViewModelTest::class,
    ForecastDaoTest::class,
    ForecastViewStateTest::class,
    SearchViewStateTest::class,
    SearchViewModelTest::class,
    WeatherDetailViewModelTest::class,
    ForecastRepositoryTest::class,
    CurrentWeatherRepositoryTest::class
)
class TestSuite
