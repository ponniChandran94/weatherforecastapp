package com.sample.app.weatherapp.di.module

import com.sample.app.weatherapp.ui.dashboard.DashboardFragment
import com.sample.app.weatherapp.ui.search.SearchFragment
import com.sample.app.weatherapp.ui.splash.SplashFragment
import com.sample.app.weatherapp.ui.weather_detail.WeatherDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector



@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashFragment

    @ContributesAndroidInjector
    abstract fun contributeDashboardFragment(): DashboardFragment

    @ContributesAndroidInjector
    abstract fun contributeWeatherDetailFragment(): WeatherDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment
}
