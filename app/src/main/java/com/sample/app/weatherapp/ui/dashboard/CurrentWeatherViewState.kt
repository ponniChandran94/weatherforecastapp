package com.sample.app.weatherapp.ui.dashboard

import com.sample.app.weatherapp.db.entity.CurrentWeatherEntity
import com.sample.app.weatherapp.utils.domain.Status


class CurrentWeatherViewState(
    val status: Status,
    val error: String? = null,
    val data: CurrentWeatherEntity? = null
) {
    fun getForecast() = data

    fun isLoading() = status == Status.LOADING

    fun getErrorMessage() = error

    fun shouldShowErrorMessage() = error != null
}
