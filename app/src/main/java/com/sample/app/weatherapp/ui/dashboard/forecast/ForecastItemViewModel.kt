package com.sample.app.weatherapp.ui.dashboard.forecast

import androidx.databinding.ObservableField
import com.sample.app.weatherapp.core.BaseViewModel
import com.sample.app.weatherapp.domain.model.ListItem
import javax.inject.Inject


class ForecastItemViewModel @Inject internal constructor() : BaseViewModel() {
    var item = ObservableField<ListItem>()
}
